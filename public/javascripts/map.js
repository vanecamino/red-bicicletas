var map = L.map('main_map').setView([-0.001797, -78.455887], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


//L.marker([-0.001797, -78.455887]).addTo(map);
//L.marker([-0.0016902, -78.42458]).addTo(map);
//L.marker([-0.0017834, -78.468741]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            console.log(bici);
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})