var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = []

Bicicleta.add = function(aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findByID = function(aBiciID) {
    var aBici = Bicicleta.allBicis.find(x => x.id==aBiciID);
    if(aBici)
        return aBici;
    elseb
        return new Error(`No existe una bicicleta con el id ${aBiciID}`)
}

Bicicleta.removeByID = function(aBiciID){
    //Bicicleta.findByID(aBiciID);
    for(var i=0; i<Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == aBiciID){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

var a = new Bicicleta(1, 'rojo', 'urbana', [-0.0018, -78.41])
var b = new Bicicleta(2, 'blanco', 'urbana', [-0.00169, -78.46])

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;
